$('.owl-carousel4').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText: ["<img src='img/truoc.png'>","<img src='img/sau.png'>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
})