$(document).ready(function(){
    $('.search_icon').click(function(){
        $('.search').addClass('visible');
    })
    $('.close_search').click(function(){
        $('.search').removeClass('visible');
    })
    var count_language = 0;
    $('.btn_toggle_language').click(function(){
        if(count_language == 0)
        {
            $('.toggle_language').show();
            count_language++;
        }
        else {
            $('.toggle_language').hide();
            count_language--;
        }
    })
    var count_navbar = 0;
    $('.btn_nav').click(function(){
        if(count_navbar == 0)
        {
            $('.navbar').show();
            count_navbar++;
        }
        else {
            $('.navbar').hide();
            count_navbar--;
        }
    })

    $('.products-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        navText: ["<img src='img/icon/owl_prev.jpg'>","<img src='img/icon/owl_next.jpg'>"],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    })
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        navText: ["<img src='img/icon/owl_prev.jpg'>","<img src='img/icon/owl_next.jpg'>"],
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    })
})