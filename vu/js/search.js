$(document).ready(function () {
    $('#btn_search').click(function () {
        $('#div_search').toggle();
        $('.txt_search').focus();
    });

    $('.btn_delete').click(function () {
        $('.txt_search').val('');
    });
    
    $('#menu_bar').click(function () {
        $('#div_search').hide();
    });
    
    let a = 0;
    $('#english').click(function () {
        if (a % 2 == 0) {
            $("#japanese").css("display", "block");
            $("#vietnamese").css("display", "block");
        } else {
            $("#japanese").css("display", "none");
            $("#vietnamese").css("display", "none");
        }
        a++;
        if (a >= 2) {
            a = 0;
        }
    });

    $('#menu_bar').click(function (event) {
        $('#menu').toggle();
    });

    $(document).click(function(event) {
        if(!$(event.target).closest('#menu, #menu_bar').length) {
            if($('#menu').is(":visible")) {
                $('#menu').hide();
            }
        }
    });
   
});
